<?php

use Examples\Cats;
use Examples\Dogs;

return [
    'Dog' => [
        'class' => Dogs::class,
        'age' => 7
    ],
    'Cat' => [
        'class' => Cats::class,
        'age' => 3
    ],
];
