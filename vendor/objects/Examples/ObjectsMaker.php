<?php

namespace Examples;

class ObjectsMaker {

    private $config;

    public function __construct($config) {
        $this->config = $config;
    }

    public function make($object) {

        $config = $this->config;

        if (isset($config[$object])) {

            $class = $config[$object]['class'];
            $age   = $config[$object]['age'];

            return new $class($age);
        } else {
            throw new ObjectsMakeException("Object $object is not configured");
        }
    }

}
