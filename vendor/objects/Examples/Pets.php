<?php

namespace Examples;

abstract class Pets {

    protected $age;

    public function __construct($age) {
        $this->age = $age;
    }

    public function __call($name, $arguments) {
        if ($name == 'class') {
            return static::class;
        }
    }

    abstract public function age();
}
