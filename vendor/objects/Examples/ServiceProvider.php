<?php

namespace Examples;

use Examples\ObjectsMaker;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use League\Flysystem\Config;
use function config_path;

class ServiceProvider extends LaravelServiceProvider {

    public function boot() {
        $this->publishes([__DIR__ . '/../config/' => config_path() . "/"], 'config');
    }

    public function register() {

        $this->app->bind(ObjectsMaker::class, function() {
            return new ObjectsMaker(Config('objects'));
        });
    }

}
