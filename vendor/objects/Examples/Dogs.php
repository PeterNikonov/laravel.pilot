<?php

namespace Examples;

class Dogs extends Pets {

    public function __construct($age) {
        parent::__construct($age);
    }

    public function age() {
        return $this->age;
    }

}
