<?php

namespace Examples;

class Cats extends Pets {

    public function __construct($age) {
        parent::__construct($age);
    }

    public function age() {
        return $this->age;
    }

}
